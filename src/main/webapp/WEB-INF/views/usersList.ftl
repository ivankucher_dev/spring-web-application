<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List of users</title>
</head>
<body>
<h1>Users List</h1>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Age</th>

    </tr>
    <#list users as user>
        <tr>
            <td><a href="user/${user.ID}">${user.ID}</a></td>
            <td>${user.username}</td>
            <td>${user.email}</td>
            <td>${user.age}</td>
            <td><a href="/SpringProject_war/delete/${user.ID}">Delete</a></td>
            <td> <a href="/SpringProject_war/edit/${user.ID}">Edit</a></td>

        </tr>
    </#list>
</table>
<a href="/SpringProject_war/addUser">Add new user</a>

</body>
</html>