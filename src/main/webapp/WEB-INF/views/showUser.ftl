<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User</title>
</head>
<body>
<h1>User Info</h1>
<table>
    <tr>
        <td>id</td>
        <td>${user.ID}</td>
        </tr>
    <tr>
        <td>Username</td>
        <td>${user.username}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>${user.email}</td>
    </tr>
    <tr>
        <td>Age</td>
        <td>${user.age}</td>
    </tr>

</table>

<br>
<h1>Cars of user ${user.username}</h1>

<table>
    <tr>
        <th>ID</th>
        <th>Model</th>
        <th>Color</th>
    </tr>
    <#list cars as car>
        <tr>
            <td>${car.id}</td>
            <td>${car.model}</td>
            <td>${car.color}</td>
            <td><a href="/SpringProject_war/user/${user.ID}/delete/${car.id}">Delete</a></td>
            <td> <a href="/SpringProject_war/user/${user.ID}/edit/${car.id}">Edit</a></td>

        </tr>
    </#list>
</table>
<a href="/SpringProject_war/user/${user.ID}/addCar">Add new car</a>

<br>
<a href="/SpringProject_war/users">Back</a>
</body>
</html>