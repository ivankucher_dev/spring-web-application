package com.iomis.SpringProject.config;


import com.iomis.SpringProject.dao.UserDAO;
import com.iomis.SpringProject.dao.UserDaoImpl;
import com.iomis.SpringProject.service.UserService;
import com.iomis.SpringProject.service.UserServiceImpl;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;


@Configuration
@ComponentScan(basePackages = {"com.iomis.SpringProject.service", "com.iomis.SpringProject.dao"})
public class SpringConfig {


    private static final String USERNAME = "root";
    private static final String PASSWORD = "I27062000v";

    @Bean
    public JdbcTemplate getJdbcTemplate(){
        JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
        return jdbcTemplate;
    }

    @Bean
    public DataSource getDataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setUrl("jdbc:mysql://localhost:3306/mydbtest?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
        ds.setUsername(USERNAME);
        ds.setPassword(PASSWORD);
        ds.setDriverClassName("com.mysql.jdbc.Driver");

        return ds;
    }





}
