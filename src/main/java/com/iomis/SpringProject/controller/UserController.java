package com.iomis.SpringProject.controller;


import com.iomis.SpringProject.entity.Car;
import com.iomis.SpringProject.entity.User;
import com.iomis.SpringProject.service.CarService;
import com.iomis.SpringProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebParam;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/")
public class UserController {


    @Autowired
    public UserService userService;
    @Autowired
    public CarService carService;

    @GetMapping
    public String ok(){
    return "index";
    }

@GetMapping("/users")
public String getAllUsers(Model model){
        model.addAttribute("users",userService.findAll());
        return "usersList";
}

    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }

    @GetMapping("/user/{id}")
    public String getUserByID(@PathVariable("id") int id, Model model){
        HashMap<String,Object> map = new HashMap<String, Object>();
        map.put("user", userService.findByID(id));
        map.put("cars",carService.findByUserID(id));
model.addAllAttributes(map);

return "showUser";
    }


    @GetMapping("/addUser")
    public String createUserPage(){
        return "createUser";
    }

    @PostMapping("/addUser")
    public String addUser(@ModelAttribute("user") User user){
        userService.save(user);
        return "redirect:/users";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id){
        userService.delete(id);
        return "redirect:/users";
    }

    @GetMapping("/edit/{id}")
    public String updateUser(@PathVariable("id") int id, Model model){
       model.addAttribute("user" , userService.findByID(id));
        return "updateUser";
    }

    @PostMapping("/updateUser")
    public String updateUser(@ModelAttribute("user") User user){
        userService.update(user);
        return "redirect:/user/"+user.getID();
    }




}
