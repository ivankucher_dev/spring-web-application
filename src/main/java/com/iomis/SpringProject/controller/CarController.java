package com.iomis.SpringProject.controller;


import com.iomis.SpringProject.entity.Car;
import com.iomis.SpringProject.entity.User;
import com.iomis.SpringProject.service.CarService;
import com.iomis.SpringProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class CarController {

    @Autowired
    public UserService userService;
    @Autowired
    public CarService carService;


    @GetMapping("/user/{id}/addCar")
    public String addCarPage(@PathVariable("id") int id , @ModelAttribute("user") User user , Model model){
        model.addAttribute("user" , userService.findByID(id));
        return "addCar";
    }

    @PostMapping("/user/{id}/addCar")
    public String addCar(@ModelAttribute("car") Car car){
        carService.addCar(car);
        return "redirect:/user/"+car.getUserid();
    }


    @GetMapping("/user/{id}/edit/{carid}")
    public String updateCar(@PathVariable("id") int id,@PathVariable("carid") int carid, Model model){
        model.addAttribute("car" ,carService.findByID(carid));
        return "updateCar";
    }

    @PostMapping("/updateCar")
    public String updateCar(@ModelAttribute("car") Car car){
        carService.update(car);
        return "redirect:/user/"+car.getUserid();
    }

    @GetMapping("/user/{id}/delete/{carid}")
    public String delete(@PathVariable("id") int id, @PathVariable("carid") int carid){
        carService.delete(carid);
        return "redirect:/user/"+id;
    }



}
