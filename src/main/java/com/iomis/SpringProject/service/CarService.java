package com.iomis.SpringProject.service;

import com.iomis.SpringProject.entity.Car;
import com.iomis.SpringProject.entity.User;

import java.util.List;

public interface CarService {

    List<Car> findAll();
    List<Car> findByUserID(int id);
    void addCar(Car car);
    void delete(int id);
    void update(Car car);
    Car findByID(int id);
}
