package com.iomis.SpringProject.service;

import com.iomis.SpringProject.dao.UserDAO;
import com.iomis.SpringProject.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    public UserDAO userDAO;

    public List<User> findAll() {
        return userDAO.fildAll();
    }

    public void delete(int id) {
        userDAO.delete(id);
    }

    public void update(User user) {
        userDAO.update(user);
    }

    public User findByID(int id) {
        return userDAO.findByID(id);
    }

    public void save(User user) {
userDAO.save(user);
    }
}
