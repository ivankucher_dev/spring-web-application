package com.iomis.SpringProject.service;

import com.iomis.SpringProject.entity.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    void delete(int id);

    void update(User user);

    User findByID(int id);

    void save(User user);
}
