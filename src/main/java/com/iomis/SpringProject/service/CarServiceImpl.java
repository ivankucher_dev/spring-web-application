package com.iomis.SpringProject.service;

import com.iomis.SpringProject.dao.CarDAO;
import com.iomis.SpringProject.dao.UserDAO;
import com.iomis.SpringProject.entity.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CarServiceImpl implements CarService {

    @Autowired
    public CarDAO carDAO;

    public List<Car> findAll() {
        return carDAO.findAll();
    }

    public List<Car> findByUserID(int id) {
        return carDAO.findByUserID(id);
    }

    public void addCar(Car car) {
        carDAO.addCar(car);
    }

    public void delete(int id) {
        carDAO.delete(id);
    }

    public void update(Car car) {
carDAO.update(car);
    }

    public Car findByID(int id) {
        return carDAO.findByID(id);
    }


}
