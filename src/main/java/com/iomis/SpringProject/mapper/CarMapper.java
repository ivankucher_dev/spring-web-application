package com.iomis.SpringProject.mapper;

import com.iomis.SpringProject.entity.Car;
import com.iomis.SpringProject.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CarMapper implements RowMapper<Car> {

    public Car mapRow(ResultSet resultSet, int i) throws SQLException {
        Car car = new Car();
       car.setId(resultSet.getInt("id"));
       car.setModel(resultSet.getString("model"));
       car.setColor(resultSet.getString("color"));
       car.setUserid(resultSet.getInt("user_id"));

        return car;
    }
}
