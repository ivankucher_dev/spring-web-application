package com.iomis.SpringProject.mapper;

import com.iomis.SpringProject.entity.User;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {

    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setAge(resultSet.getInt("age"));
        user.setUsername(resultSet.getString("name"));
        user.setEmail(resultSet.getString("email"));
        user.setID(resultSet.getInt("id"));

        return user;
    }
}
