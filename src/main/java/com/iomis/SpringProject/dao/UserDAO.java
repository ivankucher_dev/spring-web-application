package com.iomis.SpringProject.dao;

import com.iomis.SpringProject.entity.User;
import org.springframework.stereotype.Component;

import java.util.List;


public interface UserDAO {

    List<User> fildAll();

    void delete(int id);

    void update(User user);

    User findByID(int id);

    void save(User user);

}
