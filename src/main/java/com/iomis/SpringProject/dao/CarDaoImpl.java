package com.iomis.SpringProject.dao;

import com.iomis.SpringProject.entity.Car;
import com.iomis.SpringProject.entity.User;
import com.iomis.SpringProject.mapper.CarMapper;
import com.iomis.SpringProject.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CarDaoImpl implements CarDAO {

    public final JdbcTemplate jdbcTemplate;

    @Autowired
    public CarDaoImpl(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    public void addCar(Car car) {
        String sql = "INSERT into autos (model, color, user_id) VALUES (?, ? , ?)";
        jdbcTemplate.update(sql, car.getModel(),car.getColor(),car.getUserid());
    }

    public List<Car> findAll() {
        String sql = "SELECT * from autos";
        return jdbcTemplate.query(sql, new CarMapper());
    }

    public List<Car> findByUserID(int id) {
        String sql = "SELECT * from autos WHERE user_id=?";
        return jdbcTemplate.query(sql, new CarMapper(), id);
    }

    public Car findByID(int id) {
        String sql = "SELECT * from autos WHERE id = ?";
        return jdbcTemplate.queryForObject(sql,new CarMapper(),id);
    }

    public void delete(int ID) {
        String sql = "DELETE from autos WHERE id =?";
        jdbcTemplate.update(sql,ID);
    }

    public void update(Car car) {
        String sql = "UPDATE autos SET model=?,color=?,user_id=? WHERE id =?";
        jdbcTemplate.update(sql,car.getModel(), car.getColor(), car.getUserid(),car.getId());
    }
}
