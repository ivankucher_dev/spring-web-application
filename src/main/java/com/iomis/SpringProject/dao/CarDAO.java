package com.iomis.SpringProject.dao;

import com.iomis.SpringProject.entity.Car;

import java.util.List;

public interface CarDAO {

    void addCar(Car car);
    List<Car> findAll();
    List<Car> findByUserID(int id);
    Car findByID(int id);
    void delete(int id);
    void update(Car car);

}
