package com.iomis.SpringProject.dao;

import com.iomis.SpringProject.entity.User;
import com.iomis.SpringProject.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class UserDaoImpl implements UserDAO {



    public final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDaoImpl(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<User> fildAll() {

        String sql = "SELECT * from users";
        return jdbcTemplate.query(sql, new UserMapper());
    }

    public void delete(int ID) {
        String sql = "DELETE from users WHERE id =?";
       jdbcTemplate.update(sql, ID);
    }

    public void update(User user) {
        String sql = "UPDATE users SET name=?,email=?,age=? WHERE id =?";
        jdbcTemplate.update(sql, user.getUsername(), user.getEmail(),user.getAge(),user.getID());
    }

    public User findByID(int id) {
        String sql = "SELECT * from users WHERE id = ?";
        return jdbcTemplate.queryForObject(sql,new UserMapper(),id);
    }

    public void save(User user) {
        String sql = "INSERT into users (name, age, email) VALUES (?, ? , ?)";
        jdbcTemplate.update(sql, user.getUsername(),user.getAge(),user.getEmail());
    }
}
